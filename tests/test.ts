import { Fb } from '../src';
import { promisify } from 'util';
import * as fs from 'fs';

let conf: Fb.IConnectionOptions = {
     database: '/Users/marcio/Desktop/TEST_FIREBIRD_EVENTS.fdb',
     // database: 'C:\\_MARCIO_DEV\\Dev_Experiences\\test_firebird_events.fdb',
     //host: '192.168.0.195',
     host: '127.0.0.1',
     password: 'masterkey' /*process.env['FB_PASSWORD']*/,
     user: 'sysdba'
};

function delay(delay) {
     return new Promise((resolve, reject) => {
          setTimeout(() => {
               resolve();
          }, delay);
     });
}

function Resolver() {
     let r;
     let q = new Promise(resolve => (r = resolve));
     return {
          q,
          resolve: r
     };
}

let readFile = promisify(fs.readFile);

describe('Connection', function() {
     it('Should connect', () => {
          let fbConnection = new Fb.Connection({
               password: conf.password,
               user: conf.user,
               host: conf.host,
               database: conf.database
          });
          expect(async () => {
               await fbConnection.connect();
          }).not.toThrow();
     });
});

describe('Pool', () => {
     it('Should not autostart', async () => {
          let fbConnection = new Fb.Connection({
               password: conf.password,
               user: conf.user,
               host: conf.host,
               database: conf.database
          });
          let fbPool = new Fb.Pool(fbConnection, {
               autostart: false,
               min: 2,
               max: 1000
          });
          await delay(1000);
          expect(fbPool.pool.available).toBe(0);
     });

     describe('Pool without auto start', () => {
          let fbConnection: Fb.Connection,
               fbPool: Fb.Pool,
               attachment: Fb.Attachment;
          beforeAll(async () => {
               fbConnection = new Fb.Connection({
                    password: conf.password,
                    user: conf.user,
                    host: conf.host,
                    database: conf.database
               });
               fbPool = new Fb.Pool(fbConnection, {
                    autostart: false,
                    min: 2,
                    max: 1000
               });
               attachment = await fbPool.getAttachment();
          });
          describe('', () => {
               it('Can acquire connection if pool is not started', async () => {
                    expect(fbPool.pool.borrowed).toBe(1);
                    expect(fbPool.pool.available).toBe(0);
               });
          });
          describe('', () => {
               it('Should release', async () => {
                    await attachment.destroy();
                    expect(fbPool.pool.borrowed).toBe(0);
                    expect(fbPool.pool.available).toBe(1);
               });
          });
          afterAll(async () => {
               await fbPool.destroy();
          });
     });

     it('Should autostart', async () => {
          let fbConnection = new Fb.Connection({
               password: conf.password,
               user: conf.user,
               host: conf.host,
               database: conf.database
          });
          let fbPool = new Fb.Pool(fbConnection, {
               autostart: true,
               min: 2,
               max: 1000
          });
          await delay(1000);
          expect(fbPool.pool.available).toBe(2);
     });
});

describe('Pool', () => {
     it('Should start', () => {
          let fbConnection = new Fb.Connection({
               password: conf.password,
               user: conf.user,
               host: conf.host,
               database: conf.database
          });
          expect(async () => {
               await fbConnection.connect();
          }).not.toThrow();
     });
});
