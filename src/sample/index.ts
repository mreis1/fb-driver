import { Fb } from '../index';
import * as fs from 'fs';
import { promisify } from 'util';
import * as path from 'path';

let conf: Fb.IConnectionOptions = {
     database: 'C:\\_MARCIO_DEV\\04_databases\\TEST_FIREBIRD_EVENTS.FDB',
     host: '192.168.0.195',
     password: process.env['FB_PASSWORD'],
     user: 'sysdba'
};

let readFile = promisify(fs.readFile);

function start(title) {
     console.log(new Array(50).fill('>').join(''));
     if (title) {
          console.log(title);
          console.log('\n');
     }
}

function end() {
     console.log(new Array(50).fill('<').join(''));
     console.log('\n\n');
     console.log('\n\n');
}

function delay(delay) {
     return new Promise((resolve, reject) => {
          setTimeout(() => {
               resolve();
          }, delay);
     });
}

function Resolver() {
     let r;
     let q = new Promise(resolve => (r = resolve));
     return {
          q,
          resolve: r
     };
}

async function run() {
     await delay(5000);
     debugger;
     let fbConnection = new Fb.Connection({
          password: conf.password,
          user: conf.user,
          host: conf.host,
          database: conf.database
     });

     {
          try {
               start(`#0`);
               let fbPool = new Fb.Pool(
                    fbConnection,
                    {
                         autostart: false,
                         acquireTimeoutMillis: 5 * 1000
                    },
                    Fb.Connection.DEFAULT_TRANSACTION_OPTIONS
               );

               fbPool.start();
               let attachment = await fbPool.getAttachment();

               console.log('Connection obtained');

               let r = await attachment.readQuery('SELECT FIRST 2 ID FROM TEST_TABLE', [], true);
               let rows = await r.fetch({ fetchSize: 15 });
               console.log('Returned', rows);
               //endregion

               /**
                * Releasing is important!!!!
                * If the connection came from pool, then make sure to use this
                *  await attachment.rollbackReleasing();
                *  await attachment.commitReleasing()
                */
               await attachment.commitReleasing();
               await fbPool.destroy();
          } catch (e) {
          } finally {
               end();
          }
     }


     {
          {
               let attachment: Fb.Attachment;
               try {
                    start('#1');
                    attachment = await fbConnection.connect({
                         transactionOptions: {
                              accessMode: 'READ_ONLY'
                         }
                         // transactionOptions: ,
                         // attachmentOptions: {
                         //      // autoUpgradeReadWrite: true
                         // }
                    });
                    //
                    let resultSet = await attachment.readQuery('SELECT COUNT(*) FROM TEST_TABLE', [], true);
                    let rows = await resultSet.fetch();
                    console.log({ rows, isClosed: resultSet.closed });

                    await attachment.commit();

                    //region INSERT QUERY + COMMIT RETAIN
                    try {
                         await attachment.writeQuery(
                              'INSERT INTO TEST_TABLE(ID,NAME) VALUES (?,?)',
                              [Date.now(), 'foo'],
                              false
                         );
                         console.log('Insert Ok');
                         //
                         console.log('Counting...');
                         let resultSet = await attachment.readQuery(
                              'SELECT COUNT(*) FROM TEST_TABLE',
                              [],
                              true
                         );
                         console.log('Counting ok');

                         console.log('Fetching...');
                         let rows = await resultSet.fetch();
                         console.log({ rows });

                         console.log('Commiting');
                         await attachment.commitRetaining();
                    } catch (err) {
                         console.log(err);
                    } finally {
                    }
                    //endregion
               } catch (e) {
                    console.log(e);
               } finally {
                    await attachment?.destroy();
                    end();
               }
          }
     }


     //region EXECUTE PROCEDURE (ResultSet in non result set)
     {
          let attachment: Fb.Attachment;
          try {
               start('#2');
               attachment = await fbConnection.connect({
                    transactionOptions: {
                         accessMode: 'READ_ONLY'
                    }
                    // transactionOptions: ,
                    // attachmentOptions: {
                    //      // autoUpgradeReadWrite: true
                    // }
               });
               await delay(500);
               await attachment.writeQuery(
                    'EXECUTE PROCEDURE P_INSERT_ROW(?,?)',
                    [Date.now(), 'FOXTROX'],
                    false
               );
               await attachment.commitRetaining();
          } catch (err) {
          } finally {
               await attachment?.destroy();
               end();
          }
     }
     //endregion


     //region CALL TO PROCEDURE WITH INSERT (√ RESULT SET) + COMMIT RETAIN
     // @NOTE: If you
     {
          let attachment: Fb.Attachment;
          try {
               start('#3');
               attachment = await fbConnection.connect({
                    transactionOptions: {
                         accessMode: 'READ_WRITE'
                    }
               });
               await delay(500);
               await attachment.writeQuery(
                    'EXECUTE PROCEDURE P_INSERT_ROW_RETURN(?,?)',
                    [Date.now(), 'FOXTROX'],
                    false
               );
               await attachment.commitRetaining();
          } catch (err) {
          } finally {
               await attachment?.destroy();
               end();
          }
     }
     //endregion


     {
          //region SELECT * FROM P_INSERT_ROW_RETURN(?,?) + RESULT SET
          let attachment: Fb.Attachment;
          try {
               start('#4');
               attachment = await fbConnection.connect({
                    transactionOptions: {
                         accessMode: 'READ_ONLY'
                    }
               });
               let resultSet = await attachment.writeQuery(
                    'SELECT * FROM P_INSERT_ROW_RETURN(?,?)',
                    [Date.now(), 'FOXTROX'],
                    true
               );
               let rows = await resultSet.fetch();
               console.log(rows);
               await attachment.commitRetaining();
          } catch (err) {
               console.log(err);
          } finally {
               await attachment?.destroy();
               end();
          }
          //endregion
     }


     {
          //region Standard Insert statement with initial readOnly transaction
          let attachment: Fb.Attachment;
          try {
               start('#5');
               attachment = await fbConnection.connect({
                    transactionOptions: {
                         accessMode: 'READ_ONLY'
                    }
               });
               await delay(500);
               await attachment.writeQuery('INSERT INTO TEST_TABLE(ID,NAME) VALUES (?,?)', [
                    Date.now(),
                    'foo'
               ]);
               await attachment.commitRetaining();
          } catch (err) {
               console.log(err);
          } finally {
               await attachment?.destroy();
          }
          //endregion
     }

     {
          // ----- Events Example
          let attachment: Fb.Attachment;
          try {
               start('#6');
               attachment = await fbConnection.connect({
                    transactionOptions: {
                         accessMode: 'READ_ONLY'
                    }
               });
               // Create a resolver to make the process wait for
               // events close timeout
               let resolver = Resolver();
               let resolved = false;

               let q = await attachment.queueEvents(['EVENT1', 'EVENT2'], counters => {
                    console.log('Event received ', JSON.stringify(counters));
                    resolved = true;
                    resolver.resolve();
               });

               console.log('Resolving queueEvents in 5s');

               {
                    let attachment2: Fb.Attachment;
                    try {
                         // Emit events
                         attachment2 = await fbConnection.connect({
                              transactionOptions: {
                                   accessMode: 'READ_ONLY'
                              }
                         });

                         await attachment2.readQuery(
                              `
                           execute block as
                             begin
                                 post_event 'EVENT1';
                                 post_event 'EVENT1';
                                 post_event 'EVENT2';
                                 post_event 'EVENT3';
                             end
                         `,
                              [],
                              false
                         );

                         await attachment2.commit();
                    } catch (e) {
                    } finally {
                         if (attachment2) await attachment2.destroy();
                    }
               }

               await delay(1000);

               setTimeout(() => {
                    if (!resolved) {
                         resolver.resolve();
                         q.cancel();
                         resolved = true;
                    }
               }, 5000);

               console.log('Waiting for resolver to be resolved');
               await resolver.q;
               console.log('Resolver was resolved');
          } catch (err) {
               console.log(err);
          } finally {
               await attachment?.destroy();
          }
     }

     {
          start('#7');
          let attachment = await fbConnection.connect();

          try {
               try {
                    start('#7.1');
                    await attachment.writeQuery('DROP SEQUENCE "my_seq";');
               } catch (e) {
                    // do nothing
               } finally {
                    await attachment.writeQuery('CREATE SEQUENCE "my_seq";');
                    await attachment.writeQuery(
                         'ALTER SEQUENCE "my_seq"' + ' RESTART WITH ' + Date.now() + ';'
                    );
               }
               await attachment.commitRetaining();

               //region INSERT SINGLE BLOB
               {
                    start('#7.2');
                    console.log(`Start of "INSERT SINGLE BLOB"`);
                    let content = await readFile('./assets/photo-big.jpg');
                    console.log(`Photo big has ${content.byteLength}`);

                    const blob = await attachment.createBlob();
                    await blob.write(content);
                    await blob.close();

                    await attachment.writeQuery(
                         'INSERT INTO TEST_TABLE(ID, NAME, FBLOB) VALUES(NEXT VALUE FOR "my_seq",?,?)',
                         ['Foo', blob],
                         false
                    );

                    await attachment.commit();
                    console.log('end of INSERT SINGLE BLOB');
               }
               //endregion

               // region READ BLOB (using native api)
               {
                    start('#7.3');
                    let resultSet = await attachment.readQuery(
                        'SELECT FIRST 1 FBLOB, ID FROM TEST_TABLE' + ' WHERE FBLOB IS NOT NULL ORDER BY 1 DESC',
                        [],
                        true
                    );
                    let rows = await resultSet.fetch();
                    if (rows.length >= 1) {
                         let blob = rows[0][0] as Fb.Blob;
                         let blobStream = await attachment.openBlob(blob);
                         let len = await blobStream.length;
                         let totalRead = 0;
                         let blobRead = [];
                         while (totalRead !== len) {
                              let b = Buffer.alloc(65355); // 16384 * 4
                              let stat = await blobStream.read(b);
                              blobRead.push(Buffer.from(b.slice(0, stat)));
                              totalRead += stat;
                              console.log('Total Read ' + totalRead);
                         }
                         let file = path.join(process.cwd(), 'tmp_' + Date.now() + '.png');
                         console.log('Writing file to', file);
                         fs.writeFileSync(file, Buffer.concat(blobRead), {});
                    } else {
                         console.log('No results');
                    }


                    console.log('end of READ BLOB');
               }
               // endregion READ BLOB (using native api)

               // region READ BLOB (using native api)
               {
                    start('#7.4: Read Blob');
                    let resultSet = await attachment.readQuery(
                         'SELECT FIRST 1 FBLOB, ID FROM TEST_TABLE' + ' WHERE FBLOB IS NOT NULL',
                         [],
                         true
                    );
                    let rows = await resultSet.fetch();
                    if (rows.length >= 1) {
                         let blob = rows[0][0] as Fb.Blob;
                         let blobStream = await attachment.openBlob(blob);
                         let len = await blobStream.length;
                         let totalRead = 0;
                         let blobRead = [];
                         while (totalRead !== len) {
                              let b = Buffer.alloc(65355); // 16384 * 4
                              let stat = await blobStream.read(b);
                              blobRead.push(Buffer.from(b.slice(0, stat)));
                              totalRead += stat;
                              console.log('Total Read ' + totalRead);
                         }
                         let file = path.join(process.cwd(), 'tmp_' + Date.now() + '.png');
                         console.log('Writing file to', file);
                         fs.writeFileSync(file, Buffer.concat(blobRead), {});
                    } else {
                         console.log('No results');
                    }

                    end();
               }
               // endregion READ BLOB (using native api)

               // region READ MEMO (using native api)
               {
                    start('#7.5 READ MEMO');
                    let resultSet = await attachment.readQuery(
                         'SELECT FIRST 1 FMEMO, ID FROM TEST_TABLE' + ' WHERE FMEMO IS NOT NULL',
                         [],
                         true
                    );
                    let rows = await resultSet.fetch();
                    if (rows.length >= 1) {
                         let blob = rows[0][0] as Fb.Blob;
                         let blobStream = await attachment.openBlob(blob);
                         let len = await blobStream.length;
                         let totalRead = 0;
                         let blobRead = [];
                         while (totalRead !== len) {
                              let b = Buffer.alloc(65355); // 16384 * 4
                              let stat = await blobStream.read(b);
                              blobRead.push(Buffer.from(b.slice(0, stat)));
                              totalRead += stat;
                              console.log('Total Read ' + totalRead);
                         }
                         let file = path.join(process.cwd(), 'tmp_' + Date.now() + '.txt');
                         console.log('Writing file to', file);
                         fs.writeFileSync(file, Buffer.concat(blobRead), {});
                    } else {
                         console.log('No results found');
                    }

                    end();
               }
               // endregion READ MEMO (using native api)

               //region READ MEMO WITH HELPER
               {
                    start('#7.6 READ BLOB WITH HELPER');
                    let resultSet = await attachment.readQuery(
                         'SELECT FIRST 1 FBLOB, ID FROM TEST_TABLE' + ' ' +
                         ' WHERE FBLOB IS NOT NULL ORDER BY OCTET_LENGTH(FBLOB) DESC',
                         [],
                         true
                    );
                    let rows = await resultSet.fetch();
                    if (rows.length >= 1) {
                         let blob = rows[0][0] as Fb.Blob;
                         let buffer = await attachment.readBlob(blob);
                         let file = path.join(process.cwd(), 'tmp_' + Date.now() + '_helper.png');
                         console.log('Writing file to', file);
                         fs.writeFileSync(file, buffer, {});
                         console.log('end of READ BLOB');
                    } else {
                         console.log('No results');
                    }

                    end();
               }
               //endregion

               //region READ MEMO WITH HELPER
               {
                    start('#7.7 READ MEMO WITH HELPER');
                    let resultSet = await attachment.readQuery(
                         'SELECT FIRST 1 FMEMO, ID FROM TEST_TABLE' + ' WHERE FMEMO IS NOT NULL',
                         [],
                         true
                    );
                    let rows = await resultSet.fetch();
                    if (rows.length >= 1) {
                         let blob = rows[0][0] as Fb.Blob;
                         let buffer = await attachment.readBlob(blob);
                         let file = path.join(process.cwd(), 'tmp_' + Date.now() + '_helper.txt');
                         console.log('Writing file to', file);
                         fs.writeFileSync(file, buffer, {});
                         console.log('end of READ BLOB');
                    } else {
                         console.log('No results');
                    }
                    end();
               }
               //endregion

               //region READ MEMO WITH HELPER
               {
                    start('#7.8 WRITE BLOB WITH HELPER');

                    let query = 'SELECT FIRST 1 ID FROM TEST_TABLE';
                    let r1 = await attachment.readQuery(query, [], true);
                    let rows = await r1.fetch();
                    console.log(rows);
                    query = `INSERT INTO TEST_TABLE(ID,NAME,FBLOB) VALUES ((NEXT VALUE FOR "my_seq"),?, ?)`;

                    try {
                         let blobId = await attachment.createBlobWith('./assets/photo-big.jpg');
                         await attachment.writeQuery(query, ['XXX1', blobId], false);
                    } catch (err) {
                         console.log('Failed to insert from file.', err);
                    }

                    try {
                         let content = await readFile('./assets/photo-small.png');
                         console.log('Inserting image with ' + content && content.byteLength);
                         let blobId2 = await attachment.createBlobWith(content);
                         await attachment.writeQuery(query, ['YYY1', blobId2], false);
                    } catch (err) {
                         console.log('Failed to insert from buffer.', err);
                    }

                    end();
               }
               //endregion
          } catch (e) {
               console.log('Error: ', e);
          } finally {
               await attachment?.destroy();
               end();
          }

          //region MULTI BLOB INSERT (native api) WITH PREPARED STATEMENT
          {
               start('#8 MULTI BLOB INSERT WITH PREPARED STATEMENT');
               let attachment: Fb.Attachment;

               try {
                    attachment = await fbConnection.connect({
                         transactionOptions: {
                              accessMode: 'READ_ONLY'
                         }
                    });

                    // Make sure you are using a write transaction
                    await attachment.setAccessMode('READ_WRITE');

                    // Warning: You must create the BLOB using the same transaction.
                    const blob = await attachment.createBlob();

                    let content = await readFile('./assets/photo-big.jpg');
                    console.log(`Photo big has ${content.byteLength}`);
                    await blob.write(content);
                    await blob.close();

                    let query = `INSERT INTO TEST_TABLE(ID,NAME,FBLOB) VALUES ((NEXT VALUE FOR "my_seq"),?, ?)`;

                    console.log(`Preparing: ${query}`);

                    const preparedStatement = await attachment.prepare(query);

                    let dataList = [
                         ['***FOXTROX***', blob],
                         ['***FOXTROX2***', blob],
                         ['***FOXTROX3***', blob]
                    ];

                    let promStack = [];
                    let i = 0;

                    for (let item of dataList) {
                         ++i;
                         console.log('Running prepared statement' + i);
                         promStack.push(preparedStatement.executeQuery(item, false));
                    }

                    await Promise.all(promStack);
                    console.log('All prepared statements were processed!!');
                    try {
                         await attachment.destroy();
                    } catch (e) {
                         console.log('Failed to commit: ', e);
                    }
               } catch (e) {
                    console.log('Error: ', e);
               } finally {
                    await attachment?.destroy();
                    end();
               }
          }
     }
}

setTimeout(() => {
     run()
          .then(() => console.log('done'))
          .catch(err => console.log(err));
}, 15000);
