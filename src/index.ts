import {
     Attachment as _Attachment,
     ResultSet as ResultSet2,
     Statement,
     Transaction as _Transaction,
     TransactionIsolation,
     TransactionOptions,
     Blob as _Blob,
     BlobStream as _BlobStream,
     Events,
     ExecuteQueryOptions,
     PrepareOptions,
     createNativeClient,
     getDefaultLibraryFilename,
} from 'node-firebird-driver-native';

import { createPool, Factory, Options as PoolOptions, Pool as _Pool } from 'generic-pool';
import * as fs from 'fs';
import * as util from 'util';

let readFile = util.promisify(fs.readFile);

export namespace Fb {
     export interface Blob extends _Blob {}
     export interface BlobStream extends _BlobStream {}
     export interface IConnectionOptions {
          password: string;
          host: string;
          user: string;
          database: string;
     }

     export class Connection {
          // https://firebirdsql.org/manual/isql-transactions.html
          static DEFAULT_TRANSACTION_OPTIONS: TransactionOptions = {
               waitMode: 'NO_WAIT', // 'WAIT'
               // The access mode: READ_ONLY or READ_WRITE
               accessMode: 'READ_ONLY', // 'READ_WRITE'
               // All modifications made in the scope of this transaction are readable
               isolation: TransactionIsolation.SNAPSHOT,
               readCommittedMode: 'RECORD_VERSION',
               // Autocommit (When?) After query execution
               autoCommit: false,
               // Ignores limbo transactions:
               // Limbo transactions are transactions that Firebird can't determine
               // if they should be rolled back or committed
               ignoreLimbo: true
               // noAutoUndo       @todo Add
               // restartRequests
          };

          constructor(private conn: IConnectionOptions, private transactionOptions?: TransactionOptions) {
               this.transactionOptions = Object.assign(
                    {},
                    Connection.DEFAULT_TRANSACTION_OPTIONS,
                    transactionOptions || {}
               );
          }

          async connect(options?: {
               transactionOptions?: TransactionOptions;
               pool?: Pool;
               attachmentOptions?: IAttachmentOptions;
          }): Promise<Attachment> {
               options = options || {};
               const client = createNativeClient(getDefaultLibraryFilename());
               const attachment = await client.connect(`${this.conn.host}:${this.conn.database}`, {
                    password: this.conn.password,
                    username: this.conn.user
               });
               
               return new Attachment(
                    attachment,
                    {
                         ...Connection.DEFAULT_TRANSACTION_OPTIONS,
                         ...(options.transactionOptions || this.transactionOptions)
                    },
                    options.pool,
                    options.attachmentOptions
               );
          }
     }

     export class ResultSet {
          closed = false;

          constructor(private resultSet: ResultSet2) {
               if (!resultSet) {
                    throw new Error('Missing result set');
               }
          }

          async fetch(options?: { fetchSize?: number; stayOpen?: boolean }) {
               options = options || ({} as any);
               let result = await this.resultSet.fetch({
                    fetchSize: options.fetchSize
               });
               if (!(options && options.stayOpen)) {
                    await this.resultSet.close();
                    this.closed = true;
               }
               return result;
          }

          async fetchObject<T>(options?: { fetchSize?: number; stayOpen?: boolean }) {
               options = options || ({} as any);
               let result = await this.resultSet.fetchAsObject({
                    fetchSize: options.fetchSize
               });
               if (!(options && options.stayOpen)) {
                    await this.resultSet.close();
                    this.closed = true;
               }
               return result;
          }

          async close() {
               if (!this.closed) return;
               return this.resultSet.close();
          }
     }

     export interface IAttachmentOptions {
          /**
           * It will commit a read transaction and start a write READ_WRITE transaction.
           * @default = true
           * */
          autoUpgradeReadWrite?: boolean;
     }

     export class PreparedStatement {
          constructor(private statement: Statement, private attachment: Attachment) {
               if (!attachment) {
                    throw new Error('PreparedStatement requires a valid attachment');
               }
          }

          /** Disposes this statement's resources.
           * */
          dispose(): Promise<void> {
               return this.statement.dispose();
          }

          /** Executes a prepared statement that has result set. */
          async executeQuery(
               parameters?: Array<any>,
               options?: ExecuteQueryOptions,
               resultSet?: boolean
          ): Promise<ResultSet | void> {
               if (resultSet) {
                    return new ResultSet(
                         await this.statement.executeQuery(
                              this.attachment.getTransaction().transaction,
                              parameters,
                              options
                         )
                    );
               } else {
                    await this.statement.execute(
                         this.attachment.getTransaction().transaction,
                         parameters,
                         options
                    );
               }
          }
     }

     export class Attachment {
          private transaction: _Transaction;
          // Current configuration (for example, after upgrading READ_ONLY to READ_WRITE)
          private currentTransactionOptions: TransactionOptions;
          constructor(
               public attachment: _Attachment,
               // The default transactionOptions
               private transactionOptions: TransactionOptions,
               private pool?: Pool,
               private options?: IAttachmentOptions
          ) {
               this.options = options || ({} as any);
               this.ensureTransactionOptions();
               if (!('autoUpgradeReadWrite' in this.options)) {
                    this.options.autoUpgradeReadWrite = true;
               }
               if (!this.attachment || !this.transactionOptions) {
                    throw new Error('Invalid attachment init');
               }
          }

          async readQuery(sql, parameters?: Array<any>, resultSet: boolean = true): Promise<any | ResultSet> {
               await this.ensureTransaction();
               if (resultSet) {
                    return new ResultSet(
                         await this.attachment.executeQuery(this.transaction, sql, parameters)
                    );
               } else {
                    await this.attachment.execute(this.transaction, sql, parameters);
                    return void 0;
               }
          }

          /**
           * If a transaction is pending. It will attempt to commit.
           * @param accessMode
           */
          async setAccessMode(accessMode: 'READ_ONLY' | 'READ_WRITE') {
               this.ensureTransactionOptions();
               let targetTransactionOptions = { ...this.transactionOptions, accessMode: accessMode };
               if (this.transaction) {
                    if (
                         this.currentTransactionOptions.accessMode !== accessMode &&
                         accessMode === 'READ_WRITE'
                    ) {
                         try {
                              //console.log('[Attachment][setAccessMode] Upgrading transaction to READ_WRITE');
                              await this.commit();
                         } catch (e) {
                              console.log(`[Attachment][setAccessMode] Error during commit: `, e);
                         } finally {
                              await this.ensureTransaction(targetTransactionOptions);
                         }
                    }
               } else {
                    await this.ensureTransaction(targetTransactionOptions);
               }
          }
          /**
           * Runs a query that will produce data changes
           *
           * @NOTE: Don't expect a result set from EXECUTE PROCEDURE.
           *        Use select * from PROCEDURE instead!
           */
          async writeQuery(sql, parameters?: Array<any>, resultSet?: true): Promise<ResultSet>;
          async writeQuery(sql, parameters?: Array<any>, resultSet?: false): Promise<void>;
          async writeQuery(sql, parameters?: Array<any>, resultSet?: boolean): Promise<any> {
               await this.ensureTransactionOptions();
               await this.setAccessMode('READ_WRITE');
               // console.log(`Running query:  "${sql}"`, {
               //      transaction: !!this.transaction
               // });
               if (resultSet) {
                    return new ResultSet(
                         await this.attachment.executeQuery(this.transaction, sql, parameters)
                    );
               } else {
                    return await this.attachment.execute(this.transaction, sql, parameters);
               }
          }

          async commit() {
               if (this.transaction) {
                    try {
                         await this.transaction.commit();
                    } catch (e) {
                         console.log('[Attachment][commit] handling existing transaction ', e);
                    }
                    this.transaction = null;
               } else {
                    console.log('Transaction unavailable');
               }
          }

          async queueEvents(names: string[], cb: (counters: [string, number][]) => void): Promise<Events>;
          async queueEvents(...args) {
               return await this.attachment.queueEvents.apply(this.attachment, args);
          }

          async prepare(sqlStmt: string, options?: PrepareOptions): Promise<PreparedStatement>;
          async prepare(...args): Promise<PreparedStatement> {
               await this.ensureTransaction();
               let _args = [this.transaction].concat(args);
               let statement = await this.attachment.prepare.apply(this.attachment, _args);
               let attachment = this;
               return new PreparedStatement(statement, attachment);
          }

          /**
           * Create a blob handle.
           * It makes sure that the default transaction is in write mode.
           * If that's not the case, it will start in write mode.
           */
          async createBlob() {
               await this.setAccessMode('READ_WRITE');
               return this.attachment.createBlob(this.transaction);
          }

          /**
           * Creates a blob from filePath or buffer.
           * Automatically closes it.
           * @param filePath
           */
          async createBlobWith(filePath: string)
          async createBlobWith(buffer: Buffer)
          async createBlobWith(data: string|Buffer){
               if (!data) {
                    throw new Error('Invalid call to createBlobFrom.');
               }
               // Ensure that we are in write mode
               await this.setAccessMode('READ_WRITE');
               let fileContent:Buffer;
               if (typeof data === 'string') {
                    fileContent = await readFile(data);
               } else {
                    fileContent = data;
               }
               let blob = await this.attachment.createBlob(this.transaction);
               await blob.write(fileContent);
               await blob.close();
               return blob;
          }

          async openBlob(blob: Blob) {
               await this.ensureTransaction();
               return this.attachment.openBlob(this.transaction, blob);
          }

          /**
           * Tells you if a transaction exists tied to this database attachment
           */
          inTransaction(): boolean {
               return !!this.transaction;
          }

          commitRetaining() {
               return this.transaction?.commitRetaining();
          }

          async rollback() {
               await this.transaction?.rollback();
               this.transaction = null;
          }

          async commitReleasing() {
               await this.commit();
               await this.release();
          }

          async rollbackReleasing() {
               await this.commit();
               await this.release();
          }

          /**
           * Make sure that you have committed your transaction
           * before calling this method
           */
          async release() {
               if (this.pool) {
                    this.currentTransactionOptions = null;
                    return await this.pool.pool.release(this);
               }
          }

          /**
           * Disconnects the existing attachment.
           * Will commit existing transaction
           */
          async destroy() {
               let hadActiveTx = false;
               let inPool = !!this.pool;
               let commitResult: Error;
               if (this.transaction) {
                    try {
                         hadActiveTx = true;
                         await this.commit();
                    } catch (e) {
                         console.log('[attachment][destroy] ', e);
                         commitResult = e;
                         // do nothing...
                    }
               }
               if (this.pool) {
                    this.currentTransactionOptions = null;
                    return this.pool.pool.destroy(this);
               } else {
                    try {
                         await this.attachment.disconnect();
                    } catch (e) {
                         if (e.message !== 'Attachment is already disconnected.') {
                              throw e;
                         }
                    }

               }
               return {
                    inPool,
                    // Tells you if the
                    hadActiveTx,
                    commitResult
               };
          }

          async rollbackRetaining() {
               return await this.transaction?.rollbackRetaining();
          }

          private async ensureTransaction(options?: TransactionOptions) {
               if (!this.transaction) {
                    this.currentTransactionOptions = options || this.transactionOptions;
                    // console.log('Starting transaction: ', this.currentTransactionOptions);
                    this.transaction = await this.attachment.startTransaction(this.currentTransactionOptions);
                    // console.log('Transaction started');
               } else {
                    // console.log('Active transaction already exists');
               }
          }

          getTransaction() {
               return {
                    options: this.currentTransactionOptions,
                    transaction: this.transaction
               };
          }

          private ensureTransactionOptions() {
               if (!this.currentTransactionOptions) {
                    this.currentTransactionOptions = {...this.transactionOptions}
                    // console.log('Applying default transaction options');
                    // console.log(JSON.stringify(this.transactionOptions));
               }
          }

          async readBlob(column: Blob, blockSize?: number): Promise<Buffer> {
               blockSize = typeof blockSize === 'number' ? blockSize : 65355;
               let blobStream = await this.openBlob(column);
               let len = await blobStream.length;
               let totalRead = 0;
               let blobRead: Buffer[] = [];
               while (totalRead !== len) {
                    let b = Buffer.alloc(blockSize);
                    let stat = await blobStream.read(b);
                    blobRead.push(Buffer.from(b.slice(0, stat)));
                    totalRead += stat;
               }
               return Buffer.concat(blobRead);
          }
     }

     /**
      * A wrapper around GenericPool.
      */
     class PoolFactory implements Factory<Attachment> {
          constructor(
               private connection: Connection,
               private transactionOptions: TransactionOptions,
               private pool: Pool
          ) {
               if (!this.pool || !this.transactionOptions || !this.pool) {
                    throw new Error('PoolFactory construction');
               }
          }

          async create(): Promise<Attachment> {
               const att = await this.connection.connect({
                    pool: this.pool,
                    transactionOptions: this.transactionOptions
               });
               return att;
          }

          async destroy(client: Attachment) {
               if (client) {
                    try {
                         // Rollback
                         if (client.inTransaction) {
                              await client.rollback();
                         }
                    } catch (err) {
                         console.log('Rollback failed ', err);
                    } finally {
                         try {
                              // Disconnect
                              await client.attachment.disconnect();
                         } catch (err) {
                              console.log('Disconnect failed ', err);
                         } finally {
                         }
                    }
               }
          }
     }

     export interface IPoolOptions extends PoolOptions {}

     export class Pool {
          poolFactory?: PoolFactory;
          pool: _Pool<Attachment>;
          /**
           * Transaction options can be set at pooling level.
           * This means that all connections returned by this pool will be using
           */
          transactionOptions: TransactionOptions;
          poolOptions: PoolOptions = {
               autostart: false
          };
          constructor(
               connection: Connection,
               poolOptions: IPoolOptions,
               transactionOptions?: TransactionOptions
          ) {
               this.transactionOptions = transactionOptions || ({} as any);
               this.transactionOptions = {
                    ...Connection.DEFAULT_TRANSACTION_OPTIONS,
                    ...transactionOptions
               }
               this.poolOptions = {...poolOptions};
               this.poolFactory = new PoolFactory(connection, this.transactionOptions, this);
               this.pool = createPool<Attachment>(this.poolFactory, this.poolOptions);
          }

          start() {
               this.pool.start();
          }

          destroy() {
               return this.pool.drain();
          }

          getAttachment(priority?: number) {
               return this.pool.acquire(priority);
          }
     }
}
