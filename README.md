# fb-driver

A module that depends on node-firebird-drivers and aims to reduce the amount of code you need to write in order to interact with your database. Also adds db pool support.

## Install
`npm install fb-driver`

